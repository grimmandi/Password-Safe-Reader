Password Safe Reader

About:
This is a small Windows Phone 8.1 app which allows you to read Password Safe (.psafe3) files.

About Password Safe:
https://www.schneier.com/cryptography/passsafe/
