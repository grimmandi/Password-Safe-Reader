﻿using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Windows;
using System.Windows.Navigation;
using Windows.Storage;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Password_Safe_Reader__Silverlight__.ViewModel;
using Password_Safe_Reader_Constants;
using System.Windows.Controls;
using PasswordSafe;
using System.IO;
using Windows.Security.Credentials;
using Windows.Storage.Pickers;
using System.Threading.Tasks;
using Password_Safe_Reader__Silverlight_;
using Windows.ApplicationModel.Activation;
using Windows.Storage.Streams;

namespace Password_Safe_Reader__Silverlight__
{
    public partial class BetterListPage : PhoneApplicationPage
    {
        private PasswordSafeViewModel passwordSafeViewModel;
        bool _isNewPageInstance;
        IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
        List<Item> groupEntries;


        public BetterListPage()
        {
            InitializeComponent();
            passwordSafeViewModel = new PasswordSafeViewModel();
            _isNewPageInstance = true;
            groupEntries= new List<Item>();
        }

        /// <summary>
        /// Called when a page becomes the active page in a frame.
        /// </summary>
        /// <param name="e">An object that contains the event data.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            // keep open setting handling
            var localSettings = ApplicationData.Current.LocalSettings;
            bool isKeepOpenTimeouted = true;
            
            if (PhoneApplicationService.Current.State.ContainsKey(PasswordSafeConstants.PASSWORD_SAFE_STATE_NAME))
            {
                passwordSafeViewModel = (PasswordSafeViewModel)PhoneApplicationService.Current.State[PasswordSafeConstants.PASSWORD_SAFE_STATE_NAME];
                if (! passwordSafeViewModel.getSavedSafe())
                {
                    MessageBox.Show(PasswordSafeConstants.SAFE_RETRIEVAL_ERROR_TEXT);
                    NavigationService.Navigate(new Uri(PasswordSafeConstants.MAIN_PAGE_URL, UriKind.Relative));
                }
            }

            if (NavigationContext.QueryString.Keys.Contains("AddRecordPage"))
            {

            }

            if ( ! PhoneApplicationService.Current.State.ContainsKey(PasswordSafeConstants.LAST_OPENED_DATETIME_NAME))
            {
                PhoneApplicationService.Current.State[PasswordSafeConstants.LAST_OPENED_DATETIME_NAME] = DateTime.Now;
                isKeepOpenTimeouted = false;
            }
            else
            {
                // keep the safe open, unless app was closed longer than the timeout setting.
                DateTime now = DateTime.Now;
                DateTime lastOpened = (DateTime) PhoneApplicationService.Current.State[PasswordSafeConstants.LAST_OPENED_DATETIME_NAME];
                double tmp = double.Parse(localSettings.Values[PasswordSafeConstants.KEEP_OPEN_TIMEOUT_NAME].ToString());
                DateTime lastOpenedPlusTimeout = lastOpened.AddMinutes(tmp);
                if (lastOpenedPlusTimeout.CompareTo(DateTime.Now) > 0 )
                {
                    isKeepOpenTimeouted = false;
                }
                PhoneApplicationService.Current.State[PasswordSafeConstants.LAST_OPENED_DATETIME_NAME] = DateTime.Now;
            }

            if ( (! (bool) localSettings.Values[PasswordSafeConstants.KEEP_OPEN_SETTING_NAME] ||
                 isKeepOpenTimeouted ) &&
                 !_isNewPageInstance
                 )
            {
                NavigationService.Navigate(new Uri(PasswordSafeConstants.MAIN_PAGE_REOPEN_URL, UriKind.Relative));
                return;
            }

            // Set _isNewPageInstance to false. If the user navigates back to this page
            // and it has remained in memory, this value will continue to be false.
            _isNewPageInstance = false;

            // Retrieve the safe items and create the content of the Longlistselector
            generateEntryEntries();

            // Scroll to the last selected entry if possible
            if (passwordSafeViewModel.safe.Items.Contains(passwordSafeViewModel.selectedItem))
            {
                try
                {
                    EntriesSelector.ScrollTo(passwordSafeViewModel.selectedItem);
                }
                catch (Exception)
                {
                    // ScrollTo could not be done, ignore the exception and stay on top of the list.
                }
            }

            // Retrieve the safe items by their group and create the content of the Longlistselector
            generateGroupEntries();

            var app = App.Current as App;
            if (app.FileSaveContinuationArgs != null)
            {
                ContinueFileSavePicker(app.FileSaveContinuationArgs);
                app.FileSaveContinuationArgs = null;
            }

        }

        public async void ContinueFileSavePicker(FileSavePickerContinuationEventArgs fileSaveContinuationArgs)
        {
            PasswordVault pwVault = new PasswordVault();
            FileStream safeOutputStream = null;
            
            try
            {
                StorageFile file = fileSaveContinuationArgs.File;
                var tmpFile = await ApplicationData.Current.LocalFolder.CreateFileAsync("tmp-" + file.Name, CreationCollisionOption.ReplaceExisting);
                safeOutputStream = File.OpenWrite(tmpFile.Path);
                passwordSafeViewModel.safe.Save(safeOutputStream, pwVault.Retrieve(PasswordSafeConstants.PASSWORD_VAULT_RESOURCE, passwordSafeViewModel.fileName).Password);
                safeOutputStream.Close();
                byte[] tmpFileBytes = null;
                tmpFileBytes = ReadFile(tmpFile.Path);
                //IBuffer buffer = await FileIO.ReadBufferAsync(tmpFile);
                //using (DataReader dataReader = DataReader.FromBuffer(buffer))
                //{
                //    dataReader.ReadBytes(tmpFileBytes);
                //}
                await FileIO.WriteBytesAsync(file, tmpFileBytes);
                await tmpFile.DeleteAsync();
            }
            catch
            {
                MessageBox.Show(PasswordSafeConstants.SAFE_SAVE_ERROR_TEXT);
            }
            finally
            {
                if (safeOutputStream != null)
                {
                    safeOutputStream.Close();
                }
            }
        }

        public static byte[] ReadFile(string filePath)
        {
            byte[] buffer;
            FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            try
            {
                int length = (int)fileStream.Length;  // get file length
                buffer = new byte[length];            // create buffer
                int count;                            // actual number of bytes read
                int sum = 0;                          // total number of bytes read

                // read until Read method returns 0 (end of the stream has been reached)
                while ((count = fileStream.Read(buffer, sum, length - sum)) > 0)
                    sum += count;  // sum is a buffer offset for next reading
            }
            finally
            {
                fileStream.Close();
            }
            return buffer;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            PhoneApplicationService.Current.State[PasswordSafeConstants.PASSWORD_SAFE_STATE_NAME] = passwordSafeViewModel;
        }

        private List<PasswordRecordGroup<Item>> GetGroups()
        {
            return GetItemGroups(groupEntries, c => c.Group);
        }

        private static List<PasswordRecordGroup<T>> GetItemGroups<T>(IEnumerable<T> itemList, Func<T, string> getKeyFunc)
        {
            IEnumerable<PasswordRecordGroup<T>> groupList = from item in itemList
                                              group item by getKeyFunc(item) into g
                                              orderby g.Key
                                              select new PasswordRecordGroup<T>(g.Key, g);
            return groupList.ToList();
        }
        
        private void EntriesSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // store the selected item int the viewmodel
            var selectedEntry = (sender as LongListSelector).SelectedItem as Item;
            if (selectedEntry != null)
            {
                passwordSafeViewModel.selectedItem = selectedEntry;
                PhoneApplicationService.Current.State[PasswordSafeConstants.PASSWORD_SAFE_STATE_NAME] = passwordSafeViewModel;
                passwordSafeViewModel.saveSelectedItem();

                // reseting the new page instance
                _isNewPageInstance = true;

                // reseting the selected Item
                (sender as LongListSelector).SelectedItem = null;
            }
            NavigationService.Navigate(new Uri(PasswordSafeConstants.RECORD_PAGE_URL, UriKind.Relative));
        }

        private void SearchButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            SearchEntries();
        }

        private void ClearButton_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            SearchBox.Text = "";

            List<AlphaKeyGroup<Item>> DataSource = AlphaKeyGroup<Item>.CreateGroups(passwordSafeViewModel.safe.Items,
            System.Threading.Thread.CurrentThread.CurrentUICulture,
            (Item s) => { return s.Title; }, true);

            EntriesSelector.ItemsSource = DataSource;
        }

        private void SearchBox_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                SearchEntries();
                SearchBox.IsEnabled = false;
                SearchBox.IsEnabled = true;
            }
        }

        private void SearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            SearchEntries();
        }

        private void SearchEntries()
        {
            List<Item> searchedItems = new List<Item>();

            IEnumerable<Item> queryResult =
                from entry in passwordSafeViewModel.safe.Items
                where entry.Title.ToLower().Contains(SearchBox.Text.ToLower())
                select entry;

            foreach (var item in queryResult)
            {
                searchedItems.Add(item);
            }

            List<AlphaKeyGroup<Item>> DataSource = AlphaKeyGroup<Item>.CreateGroups(searchedItems,
            System.Threading.Thread.CurrentThread.CurrentUICulture,
            (Item s) => { return s.Title; }, true);

            EntriesSelector.ItemsSource = DataSource;
        }

        private void AppBarAdd_Click(object sender, EventArgs e)
        {
            // reseting the new page instance
            _isNewPageInstance = true;
            NavigationService.Navigate(new Uri(PasswordSafeConstants.ADDRECORD_PAGE_URL, UriKind.Relative));
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            var selectedEntry = (sender as MenuItem).DataContext as Item;
            if (selectedEntry != null)
            {
                passwordSafeViewModel.safe.Items.Remove(selectedEntry);
                PhoneApplicationService.Current.State[PasswordSafeConstants.PASSWORD_SAFE_STATE_NAME] = passwordSafeViewModel;
                passwordSafeViewModel.saveSafe();
            }
            generateEntryEntries();

            generateGroupEntries();

            
        }

        /// <summary>
        /// Generates the datasource for the Longlistselector
        /// </summary>
        private void generateEntryEntries()
        {
            List<AlphaKeyGroup<Item>> DataSource = AlphaKeyGroup<Item>.CreateGroups(passwordSafeViewModel.safe.Items,
                System.Threading.Thread.CurrentThread.CurrentUICulture,
                (Item s) => { return s.Title; }, true);

            EntriesSelector.ItemsSource = DataSource;
        }
        /// <summary>
        /// Generates the datasource for the Group Longlistselector
        /// </summary>
        private void generateGroupEntries()
        {
            groupEntries = new List<Item>();
            foreach (Item item in passwordSafeViewModel.safe.Items)
            {
                if (!string.IsNullOrEmpty(item.Group))
                {
                    groupEntries.Add(item);
                }
            }
            GroupEntriesSelector.ItemsSource = GetGroups();
        }

        private async void AppBarSave_Click(object sender, EventArgs e)
        {
            PasswordVault pwVault = new PasswordVault();
            FileStream safeOutputStream = null;
            try
            {
                StorageFile file = await ApplicationData.Current.LocalFolder.GetFileAsync(passwordSafeViewModel.fileName);
                safeOutputStream = File.Open(file.Path, FileMode.Open);
                passwordSafeViewModel.safe.Save(safeOutputStream, pwVault.Retrieve(PasswordSafeConstants.PASSWORD_VAULT_RESOURCE, passwordSafeViewModel.fileName).Password);
            }
            catch
            {
                MessageBox.Show(PasswordSafeConstants.SAFE_SAVE_ERROR_TEXT);
            }
            finally
            {
                if (safeOutputStream != null)
                {
                    safeOutputStream.Close();
                }
            }
        }

        private void AppBarSaveAs_Click(object sender, EventArgs e)
        {
            FileSavePicker savePicker = new FileSavePicker();
            savePicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
            // Dropdown of file types the user can save the file as
            savePicker.FileTypeChoices.Add("Password Safe Database", new List<string>() { ".psafe3" });
            // Default file name if the user does not type one in or select a file to replace
            savePicker.SuggestedFileName = passwordSafeViewModel.fileName;

            savePicker.PickSaveFileAndContinue();
        }
    }
}