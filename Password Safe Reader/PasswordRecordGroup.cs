﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Password_Safe_Reader__Silverlight__
{
    public class PasswordRecordGroup<T> : List<T>
    {
        public PasswordRecordGroup(string name, IEnumerable<T> items)
            : base(items)
        {
            Title = name;
        }

        public string Title
        {
            get;
            set;
        }
    }
}
