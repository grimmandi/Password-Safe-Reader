﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Windows.Storage;
using System.IO;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Windows.Security.Credentials;
using Password_Safe_Reader_Constants;

namespace Password_Safe_Reader__Silverlight__
{
    public partial class PasswordSettingsPage : PhoneApplicationPage
    {
        private ObservableCollection<String> Items { get; set; }

        public PasswordSettingsPage()
        {
            InitializeComponent();
            // on first open init the keep open settings
            var localSettings = ApplicationData.Current.LocalSettings;

        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            Items = new ObservableCollection<String>();

            IReadOnlyList<StorageFile> filesInFolder = await ApplicationData.Current.LocalFolder.GetFilesAsync();
            foreach (StorageFile file in filesInFolder)
            {
                if (file.FileType.Equals(".psafe3"))
                {
                    FileInfo fileInfo = new FileInfo(file.Path);
                    Items.Add(file.Name);
                }
            }

            PasswordSafePicker.ItemsSource = Items;


        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
        }

        private bool verifyContents()
        {
            if (PasswordSafePicker.SelectedIndex.Equals(null) ||
                SafePasswordBox.Password == "" ||
                QuickPasswordBox.Password == "")
            {
                return false;
            }
            return true;
        }

        private void AppBarDeleteAll_Click(object sender, EventArgs e)
        {
            deleteSavedPasswords(true);
        }

        private void deleteSavedPasswords(bool all=false)
        {
            PasswordVault pwVault = new PasswordVault();
            foreach (var item in pwVault.RetrieveAll())
            {
                if (item.UserName.EndsWith(PasswordSafeConstants.QUICK_OPEN_EXTENSION) ||
                    item.UserName.EndsWith(PasswordSafeConstants.QUICK_OPEN_PASSWORD_EXTENSION))
                {
                    if (all || item.UserName.StartsWith(PasswordSafePicker.SelectedItem as string))
                    {
                        pwVault.Remove(item);
                    }
                }
            }
        }

        private void AppBarSave_Click(object sender, EventArgs e)
        {
            saveChanges();
        }

        private void saveChanges()
        {
            if (!verifyContents())
            {
                MessageBox.Show(PasswordSafeConstants.QUICK_OPEN_VERIFY_FAIL);
            }
            else
            {
                PasswordVault pwVault = new PasswordVault();
                pwVault.Add(new PasswordCredential(PasswordSafeConstants.PASSWORD_VAULT_RESOURCE, PasswordSafePicker.SelectedItem + PasswordSafeConstants.QUICK_OPEN_PASSWORD_EXTENSION, SafePasswordBox.Password));
                pwVault.Add(new PasswordCredential(PasswordSafeConstants.PASSWORD_VAULT_RESOURCE, PasswordSafePicker.SelectedItem + PasswordSafeConstants.QUICK_OPEN_EXTENSION, QuickPasswordBox.Password));
            }
            QuickPasswordBox.Focus();
            QuickPasswordBox.IsEnabled = false;
            QuickPasswordBox.IsEnabled = true;
            SafePasswordBox.Password = "";
            QuickPasswordBox.Password = "";
            MessageBox.Show(PasswordSafeConstants.QUICK_OPEN_SET_TEXT + PasswordSafePicker.SelectedItem);
        }

        private void AppBarDelete_Click(object sender, EventArgs e)
        {
            deleteSavedPasswords();
        }

        private void SafePasswordBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                QuickPasswordBox.Focus();
            }
        }

        private void QuickPasswordBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                saveChanges();
            }
        }

        private void HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri(PasswordSafeConstants.INFO_PAGE_URL, UriKind.Relative));
        }
    }
}