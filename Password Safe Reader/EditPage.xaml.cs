﻿using System;
using System.Windows;
using Microsoft.Phone.Controls;
using System.Windows.Input;
using System.Windows.Navigation;
using Windows.Storage;
using Microsoft.Phone.Shell;
using Password_Safe_Reader__Silverlight__.ViewModel;
using Password_Safe_Reader_Constants;

namespace Password_Safe_Reader__Silverlight__
{
    public partial class EditPage : PhoneApplicationPage
    {
        private string selectedSafe;
        public EditPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            selectedSafe = (string)PhoneApplicationService.Current.State[PasswordSafeConstants.EDIT_SAFE_NAME];
            var localSettings = ApplicationData.Current.LocalSettings;
            if (localSettings.Values.ContainsKey(selectedSafe))
            {
                TitleTextBox.Text = (string)localSettings.Values[selectedSafe];
                FileNameTextBox.Text = selectedSafe;
            }            
        }

        private async void Save_Clicked(object sender, RoutedEventArgs e)
        {
            selectedSafe = (string)PhoneApplicationService.Current.State[PasswordSafeConstants.EDIT_SAFE_NAME];
            var localSettings = ApplicationData.Current.LocalSettings;
            if (localSettings.Values.ContainsKey(selectedSafe))
            {
                localSettings.Values[selectedSafe] = TitleTextBox.Text;
            }
            else
            {
                localSettings.Values.Add(selectedSafe, TitleTextBox.Text);
            }
            try
            {
                if (selectedSafe != FileNameTextBox.Text)
                {
                    var selectedSafeFile = await ApplicationData.Current.LocalFolder.GetFileAsync(selectedSafe);
                    await selectedSafeFile.RenameAsync(FileNameTextBox.Text, NameCollisionOption.GenerateUniqueName);
                }
            }
            catch (Exception)
            {
                MessageBox.Show(PasswordSafeConstants.SAFE_FILE_RENAME_ERROR_TEXT);
            }

            NavigationService.Navigate(new Uri(PasswordSafeConstants.MAIN_PAGE_URL, UriKind.Relative));
        }

        private void TitleTextBox_OnKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Focus();
            }
        }

        private void FileNameTextBox_OnKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Focus();
            }
        }
    }
}