﻿using System.Windows;
using Microsoft.Phone.Controls;
using System.IO.IsolatedStorage;
using Microsoft.Phone.Shell;
using System.Windows.Navigation;
using Password_Safe_Reader_Constants;
using System;

namespace Password_Safe_Reader__Silverlight__
{
    public partial class SettingsPage : PhoneApplicationPage
    {
        public SettingsPage()
        {
            InitializeComponent();
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;

            if (localSettings.Values.ContainsKey(PasswordSafeConstants.KEEP_OPEN_SETTING_NAME))
            {
                checkBox.IsChecked = (bool) localSettings.Values[PasswordSafeConstants.KEEP_OPEN_SETTING_NAME];
            }
            else
            {
                checkBox.IsChecked = false;
            }

        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            TimeoutTextBox.Text = localSettings.Values[PasswordSafeConstants.KEEP_OPEN_TIMEOUT_NAME].ToString();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
        }

        private void checkBox_Checked(object sender, RoutedEventArgs e)
        {
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            localSettings.Values[PasswordSafeConstants.KEEP_OPEN_SETTING_NAME] = true;
            TimeoutTextBox.IsEnabled = true;
            TimeoutTextBox.Focus();
            TimeoutTextBox.SelectAll();
        }

        private void checkBox_Unchecked(object sender, RoutedEventArgs e)
        {
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            localSettings.Values[PasswordSafeConstants.KEEP_OPEN_SETTING_NAME] = false;
            TimeoutTextBox.IsEnabled = false;
        }

        private void TimeoutTextBox_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            int timeout = 5;
            if (int.TryParse(TimeoutTextBox.Text, out timeout))
            {
                var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
                localSettings.Values[PasswordSafeConstants.KEEP_OPEN_TIMEOUT_NAME] = timeout;
            }
        }

        private void PasswordSettingsButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new System.Uri(PasswordSafeConstants.PASSWORDSETTINGS_PAGE_URL, UriKind.Relative));
        }
    }
}