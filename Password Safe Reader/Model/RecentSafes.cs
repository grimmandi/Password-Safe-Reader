﻿using System;
using System.ComponentModel;

namespace Password_Safe_Reader__Silverlight_.Model
{
    public class RecentSafes
    {
        private string _name;
        private string _title;
        private DateTimeOffset _date;
        // The name of the recent Safe.
        public string FileName
        {
            get { return _name; }
            set { _name = value; }
        }

        // The Title of the recent Safe.
        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        // The Modifydate recent Safe.
        public DateTimeOffset Date
        {
            get { return _date; }
            set { _date = value; }
        }

        public RecentSafes()
        {

        }

        // override object.Equals
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            if (((RecentSafes)obj).FileName == this.FileName)
            {
                return true;
            }

            return base.Equals(obj);
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            // TODO: write your implementation of GetHashCode() here
            return base.GetHashCode();
        }

        public RecentSafes(string name, string title)
        {
            FileName = name;
            Title = title;
        }

        public RecentSafes(string name, string title, DateTimeOffset date)
        {
            FileName = name;
            Title = title;
            Date = date;
        }


        public RecentSafes(string name)
        {
            FileName = name;
            Title = name;
        }


        public RecentSafes GetCopy()
        {
            RecentSafes copy = (RecentSafes)this.MemberwiseClone();
            return copy;
        }
    }
}
