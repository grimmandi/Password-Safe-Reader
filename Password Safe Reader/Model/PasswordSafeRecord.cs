#region License
/*
 *  PasswordSafe Database Reader/Writer
 *
 *  Copyright (C) 2007 Svante Seleborg
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *  If you'd like to license this program under any other terms than the
 *  above, please contact the author and copyright holder.
 *
 *  Contact: mailto:svante@axantum.com
 */
#endregion

using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Axantum.PasswordSafe
{
    /// <summary>
    /// A database password record
    /// </summary>
    public class PasswordSafeRecord : PasswordSafeRecordBase, INotifyPropertyChanged, IComparable
    {

        private Guid _uuid;

        /// <summary>
        /// Gets or sets the UUID (GUID) used for synchronization etc
        /// </summary>
        /// <value>The UUID.</value>
        public Guid Uuid
        {
            get { return _uuid; }
            set { _uuid = value; }
        }

        private string _group;

        /// <summary>
        /// Gets or sets the group.
        /// </summary>
        /// <value>The group.</value>
        public string Group
        {
            get { return _group; }
            set { _group = value; }
        }

        private string _title;

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>The title.</value>
        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        private string _user;

        /// <summary>
        /// Gets or sets the user.
        /// </summary>
        /// <value>The user.</value>
        public string User
        {
            get { return _user; }
            set { _user = value; }
        }

        private string _notes;

        /// <summary>
        /// Gets or sets the notes.
        /// </summary>
        /// <value>The notes.</value>
        public string Notes
        {
            get { return _notes; }
            set { _notes = value; }
        }

        private string _passwordValue = String.Empty;

        /// <summary>
        /// Gets or sets the password value. Don't use this to get the passord, use the Password property.
        /// </summary>
        /// <value>The password value.</value>
        internal string PasswordValue
        {
            get { return _passwordValue; }
            set { _passwordValue = value; }
        }

        private DateTime _timeRecordCreatedUtc = DateTime.MinValue;

        /// <summary>
        /// Gets or sets the time the record was created.
        /// </summary>
        /// <value>The time record created.</value>
        public DateTime TimeRecordCreatedUtc
        {
            get { return _timeRecordCreatedUtc; }
            set { _timeRecordCreatedUtc = value; }
        }

        private DateTime _timeRecordAccessedUtc = DateTime.MinValue;

        /// <summary>
        /// Gets or sets the time the record was accessed (UTC).
        /// </summary>
        /// <value>The date and time</value>
        public DateTime TimeRecordAccessedUtc
        {
            get { return _timeRecordAccessedUtc; }
            set { _timeRecordAccessedUtc = value; }
        }

        private DateTime _timeRecordModifiedUtc = DateTime.MinValue;

        /// <summary>
        /// Gets or sets the time the record was modified (UTC).
        /// </summary>
        /// <value>The date and time</value>
        public DateTime TimeRecordModifiedUtc
        {
            get { return _timeRecordModifiedUtc; }
            set { _timeRecordModifiedUtc = value; }
        }

        private DateTime _timePasswordExpiresUtc = DateTime.MaxValue;

        /// <summary>
        /// Gets or sets the time the password expires (UTC). (Use the 'Password' property)
        /// </summary>
        /// <value>The date and time</value>
        internal DateTime TimePasswordExpiresUtc
        {
            get { return _timePasswordExpiresUtc; }
            set { _timePasswordExpiresUtc = value; }
        }

        private DateTime _timePasswordModifiedUtc = DateTime.MinValue;

        /// <summary>
        /// Gets or sets the time the password was modified (UTC). (Use the 'Password' property)
        /// </summary>
        /// <value>The date and time</value>
        internal DateTime TimePasswordModifiedUtc
        {
            get { return _timePasswordModifiedUtc; }
            set { _timePasswordModifiedUtc = value; }
        }

        /// <summary>
        /// Gets the password.
        /// </summary>
        /// <value>The password.</value>
        public PasswordSafePassword Password
        {
            get
            {
                return new PasswordSafePassword(PasswordValue, TimePasswordModifiedUtc, TimePasswordExpiresUtc);
            }
        }

        private string _resourceLocator;

        /// <summary>
        /// Gets or sets the URL.
        /// </summary>
        /// <value>The URL.</value>
        public string ResourceLocator
        {
            get { return _resourceLocator; }
            set { _resourceLocator = value; }
        }

        private string _autoType;

        /// <summary>
        /// Gets or sets the auto-type string. [Not sure if this is really a good idea, it sounds to me like
        /// a vulnerability vector...] You may want to be careful about using the contents.
        /// </summary>
        /// <value>The string to automatically type</value>
        public string AutoType
        {
            get { return _autoType; }
            set { _autoType = value; }
        }

        private PasswordSafePasswordHistory _passwordHistory;

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets the password history.
        /// </summary>
        /// <value>The password history.</value>
        public PasswordSafePasswordHistory PasswordHistory
        {
            get { return _passwordHistory; }
            set { _passwordHistory = value; }
        }

        public PasswordSafeRecord GetCopy()
        {
            PasswordSafeRecord copy = (PasswordSafeRecord)this.MemberwiseClone();
            return copy;
        }

        public int CompareTo(object obj)
        {
            PasswordSafeRecord record = obj as PasswordSafeRecord;
            if (record == null)
            {
                throw new ArgumentException("Object is not Preson");
            }
            return Title.CompareTo(record.Title);
        }

        public PasswordSafeRecord()
        {
            
        }
    }
}
