/*
 * ---------------------------------------------------------
 * SHA256Wrappers.cs
 * 
 * by Alphons van der Heijden
 * --------------------------------------------------------- 
 * Comments:
 *   Some simple wrappers so it works 
 *   as in System.Security.Cryptography
 *   the Axantum.PasswordSafe likes it this way
 *   especialy in PasswordSafeReader.cs
 * ---------------------------------------------------------
 * 
 */
using System;

namespace Helpers.PasswordSafe
{
	/// <summary>
	/// Wrapper to make it behave the same way as Axantum.PasswordSafe likes
	/// </summary>
	public class SHA256 : IDisposable
	{
		/// <summary>
		/// helper function not to rely on System.Text.ASCIIEncoder
		/// </summary>
		public static byte[] ASCIIEncoder(string s)
		{
			byte[] ascii = new byte[s.Length];
			for (int i = 0; i < s.Length; i++)
			{
				ascii[i] = (byte)s[i];
			}
			return ascii;
		}

		public SHA256()
		{
			Clear();
		}

		public static SHA256 Create()
		{
			return new SHA256();
		}

		public byte[] ComputeHash(byte[] input)
		{
			return SHA256Class.MessageSHA256(input);
		}

		void System.IDisposable.Dispose()
		{
		}

		private byte[] m_StartBlock;
		private byte[] m_Hash;

		public int TransformBlock(byte[] inputBuffer, int inputOffset, int inputCount, byte[] outputBuffer, int outputOffset)
		{
			m_StartBlock = new byte[inputCount];
			Array.Copy(inputBuffer, inputOffset, m_StartBlock, 0, inputCount);
			
			//byte[] data = new byte[m_StartBlock.Length + inputCount];
			//Array.Copy(m_StartBlock, 0, data, 0, m_StartBlock.Length);
			//Array.Copy(inputBuffer, inputOffset, data, m_StartBlock.Length, inputCount);
			//m_StartBlock = SHA256Class.MessageSHA256(data);

			return inputCount;
		}

		public byte[] TransformFinalBlock(byte[] inputBuffer, int inputOffset, int inputCount)
		{
			byte[] data = new byte[m_StartBlock.Length + inputCount];
			Array.Copy(m_StartBlock, 0, data, 0, m_StartBlock.Length);
			Array.Copy(inputBuffer, inputOffset, data, m_StartBlock.Length, inputCount);

			data = SHA256Class.MessageSHA256(data);

			m_Hash = new byte[32];
			Array.Copy(data, 0, m_Hash, 0, 32);

			return inputBuffer;
		}

		public byte[] Hash
		{
			get
			{
				return m_Hash;
			}
		}

		public void Clear()
		{
			m_StartBlock = new byte[0];
			m_Hash = new byte[0];
		}
	}

	public class SHA256Managed : SHA256
	{
	}

	/// <summary>
	/// More or less a dummy class,
	/// The TransformBlock only does init the startblock
	/// it can not be used as it should be
	/// therefore the compare in ReadHmac() 
	/// in PasswordSafeReader.cs fails!!
	/// </summary>
	public class HMACSHA256 : SHA256
	{
		public HMACSHA256(byte[] hmacKeyBytes)
		{
			this.TransformBlock(hmacKeyBytes, 0, hmacKeyBytes.Length, null, 0);
		}
	}

}
