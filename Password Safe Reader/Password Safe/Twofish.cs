using System;
using System.Diagnostics;
using System.Security.Cryptography;

namespace ManyMonkeys.Cryptography
{
	/// <summary>
	/// Summary description for Twofish encryption algorithm of which more information can be found at http://www.counterpane.com/twofish.html. 
	/// This is based on the MS cryptographic framework and can therefore be used in place of the RijndaelManaged classes
	/// provided by MS in System.Security.Cryptography and the other related classes
	/// </summary>
	public sealed class Twofish : SymmetricAlgorithm
	{
		/// <summary>
		/// This is the Twofish constructor.
		/// </summary>
		public Twofish()
		{
			this.LegalKeySizesValue = new KeySizes[]{new KeySizes(128,256,64)}; // this allows us to have 128,192,256 key sizes

			this.LegalBlockSizesValue = new KeySizes[]{new KeySizes(128,128,0)}; // this is in bits - typical of MS - always 16 bytes

			this.BlockSize = 128; // set this to 16 bytes we cannot have any other value
			this.KeySize = 128; // in bits - this can be changed to 128,192,256

            this.Padding = PaddingMode.Zeros;


            this.Mode = CipherMode.ECB;

		}

        public enum CipherMode
        {
            //
            // Summary:
            //     The Cipher Block Chaining (CBC) mode introduces feedback. Before each plain text
            //     block is encrypted, it is combined with the cipher text of the previous block
            //     by a bitwise exclusive OR operation. This ensures that even if the plain text
            //     contains many identical blocks, they will each encrypt to a different cipher
            //     text block. The initialization vector is combined with the first plain text block
            //     by a bitwise exclusive OR operation before the block is encrypted. If a single
            //     bit of the cipher text block is mangled, the corresponding plain text block will
            //     also be mangled. In addition, a bit in the subsequent block, in the same position
            //     as the original mangled bit, will be mangled.
            CBC = 1,
            //
            // Summary:
            //     The Electronic Codebook (ECB) mode encrypts each block individually. Any blocks
            //     of plain text that are identical and in the same message, or that are in a different
            //     message encrypted with the same key, will be transformed into identical cipher
            //     text blocks. Important: This mode is not recommended because it opens the door
            //     for multiple security exploits. If the plain text to be encrypted contains substantial
            //     repetition, it is feasible for the cipher text to be broken one block at a time.
            //     It is also possible to use block analysis to determine the encryption key. Also,
            //     an active adversary can substitute and exchange individual blocks without detection,
            //     which allows blocks to be saved and inserted into the stream at other points
            //     without detection.
            ECB = 2,
            //
            // Summary:
            //     The Output Feedback (OFB) mode processes small increments of plain text into
            //     cipher text instead of processing an entire block at a time. This mode is similar
            //     to CFB; the only difference between the two modes is the way that the shift register
            //     is filled. If a bit in the cipher text is mangled, the corresponding bit of plain
            //     text will be mangled. However, if there are extra or missing bits from the cipher
            //     text, the plain text will be mangled from that point on.
            OFB = 3,
            //
            // Summary:
            //     The Cipher Feedback (CFB) mode processes small increments of plain text into
            //     cipher text, instead of processing an entire block at a time. This mode uses
            //     a shift register that is one block in length and is divided into sections. For
            //     example, if the block size is 8 bytes, with one byte processed at a time, the
            //     shift register is divided into eight sections. If a bit in the cipher text is
            //     mangled, one plain text bit is mangled and the shift register is corrupted. This
            //     results in the next several plain text increments being mangled until the bad
            //     bit is shifted out of the shift register. The default feedback size can vary
            //     by algorithm, but is typically either 8 bits or the number of bits of the block
            //     size. You can alter the number of feedback bits by using the System.Security.Cryptography.SymmetricAlgorithm.FeedbackSize
            //     property. Algorithms that support CFB use this property to set the feedback.
            CFB = 4,
            //
            // Summary:
            //     The Cipher Text Stealing (CTS) mode handles any length of plain text and produces
            //     cipher text whose length matches the plain text length. This mode behaves like
            //     the CBC mode for all but the last two blocks of the plain text.
            CTS = 5
        }

        public enum PaddingMode
        {
            //
            // Summary:
            //     No padding is done.
            None = 1,
            //
            // Summary:
            //     The PKCS #7 padding string consists of a sequence of bytes, each of which is
            //     equal to the total number of padding bytes added.
            PKCS7 = 2,
            //
            // Summary:
            //     The padding string consists of bytes set to zero.
            Zeros = 3,
            //
            // Summary:
            //     The ANSIX923 padding string consists of a sequence of bytes filled with zeros
            //     before the length.
            ANSIX923 = 4,
            //
            // Summary:
            //     The ISO10126 padding string consists of random data before the length.
            ISO10126 = 5
        }

        public PaddingMode Padding { get; set; }

        /// <summary>
        /// Creates an object that supports ICryptoTransform that can be used to encrypt data using the Twofish encryption algorithm.
        /// </summary>
        /// <param name="key">A byte array that contains a key. The length of this key should be equal to the KeySize property</param>
        /// <param name="iv">A byte array that contains an initialization vector. The length of this IV should be equal to the BlockSize property</param>
        public override ICryptoTransform CreateEncryptor(byte[] key, byte[] iv)
		{
			Key = key; // this appears to make a new copy
            
			if (Mode == CipherMode.CBC)
				IV = iv;
			
			return new TwofishEncryption(KeySize, ref KeyValue, ref IVValue, ModeValue, TwofishBase.EncryptionDirection.Encrypting);
		}

		/// <summary>
		/// Creates an object that supports ICryptoTransform that can be used to decrypt data using the Twofish encryption algorithm.
		/// </summary>
		/// <param name="key">A byte array that contains a key. The length of this key should be equal to the KeySize property</param>
		/// <param name="iv">A byte array that contains an initialization vector. The length of this IV should be equal to the BlockSize property</param>
		public override ICryptoTransform CreateDecryptor(byte[] key, byte[] iv)
		{
			Key = key;

			if (Mode == CipherMode.CBC)
				IV = iv;

			return new TwofishEncryption(KeySize, ref KeyValue, ref IVValue, ModeValue, TwofishBase.EncryptionDirection.Decrypting);
		}

		/// <summary>
		/// Generates a random initialization Vector (IV). 
		/// </summary>
		public override void GenerateIV()
		{
			IV = new byte[16]{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		}

		/// <summary>
		/// Generates a random Key. This is only really useful in testing scenarios.
		/// </summary>
		public override void GenerateKey()
		{
			Key = new byte[KeySize/8];

			// set the array to all 0 - implement a random key generation mechanism later probably based on PRNG
			for (int i=Key.GetLowerBound(0);i<Key.GetUpperBound(0);i++)
			{
				Key[i]=0;
			}
		}

		/// <summary>
		/// Override the Set method on this property so that we only support CBC and EBC
		/// </summary>
		public CipherMode Mode
		{
			set
			{
				switch (value)
				{
					case CipherMode.CBC:
						break;
					case CipherMode.ECB:
						break;
					default:
						throw (new CryptographicException("Specified CipherMode is not supported."));
				}
				this.ModeValue = value;
			}
            get { return this.ModeValue; }
		}

        public CipherMode ModeValue { get; private set; }
    }
}
