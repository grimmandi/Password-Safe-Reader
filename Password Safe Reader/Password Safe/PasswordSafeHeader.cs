#region License
/*
 *  PasswordSafe Database Reader/Writer
 *
 *  Copyright (C) 2007 Svante Seleborg
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *  If you'd like to license this program under any other terms than the
 *  above, please contact the author and copyright holder.
 *
 *  Contact: mailto:svante@axantum.com
 */
#endregion

using System;
using System.Collections.Generic;

namespace Axantum.PasswordSafe
{
    /// <summary>
    /// Represents the header record
    /// </summary>
    public class PasswordSafeHeader : PasswordSafeRecordBase
    {
        private byte _minorVersion;

        /// <summary>
        /// Gets or sets the minor version.
        /// </summary>
        /// <value>The minor version.</value>
        public byte MinorVersion
        {
            get { return _minorVersion; }
            set { _minorVersion = value; }
        }

        private byte _majorVersion;

        /// <summary>
        /// Gets or sets the major version.
        /// </summary>
        /// <value>The major version.</value>
        public byte MajorVersion
        {
            get { return _majorVersion; }
            set { _majorVersion = value; }
        }

        private Guid _uuid;

        /// <summary>
        /// Gets or sets the UUID for the database.
        /// </summary>
        /// <value>The UUID.</value>
        public Guid Uuid
        {
            get { return _uuid; }
            set { _uuid = value; }
        }

        private string _nonDefaultUserPrefs;

        /// <summary>
        /// Gets or sets the non default user prefs.
        /// </summary>
        /// <value>The non default user prefs.</value>
        public string NonDefaultUserPrefs
        {
            get { return _nonDefaultUserPrefs; }
            set { _nonDefaultUserPrefs = value; }
        }

        private List<bool> _treeDisplayStatus = new List<bool>();

        /// <summary>
        /// Gets the tree display status. Used to remember the GUI state.
        /// </summary>
        /// <value>The tree display status.</value>
        public IList<bool> TreeDisplayStatus
        {
            get { return _treeDisplayStatus; }
        }

        /// <summary>
        /// Adds another status to the tree display status.
        /// </summary>
        /// <param name="value">if set to <c>true</c> [value].</param>
        public void TreeDisplayStatusAdd(bool value)
        {
            _treeDisplayStatus.Add(value);
        }

        private string _lastSavedBy;

        /// <summary>
        /// Gets or sets the last saved by user.
        /// </summary>
        /// <value>Last saved by.</value>
        public string LastSavedBy
        {
            get { return _lastSavedBy; }
            set { _lastSavedBy = value; }
        }

        private string _lastSavedOn;

        /// <summary>
        /// Gets or sets the date and time the database was last saved.
        /// </summary>
        /// <value>When last saved</value>
        public string LastSavedOn
        {
            get { return _lastSavedOn; }
            set { _lastSavedOn = value; }
        }

        private string _whatLastSaved;

        /// <summary>
        /// Gets or sets the the application that saved last.
        /// </summary>
        /// <value>A string identifying the application.</value>
        public string WhatLastSaved
        {
            get { return _whatLastSaved; }
            set { _whatLastSaved = value; }
        }

        private string _lastUpdateUser;

        /// <summary>
        /// Gets or sets the last update user.
        /// </summary>
        /// <value>The last update user.</value>
        public string LastUpdateUser
        {
            get { return _lastUpdateUser; }
            set { _lastUpdateUser = value; }
        }

        private string _lastUpdateHost;

        /// <summary>
        /// Gets or sets the last update host.
        /// </summary>
        /// <value>The last update host.</value>
        public string LastUpdateHost
        {
            get { return _lastUpdateHost; }
            set { _lastUpdateHost = value; }
        }

        private DateTime _lastUpdateTimeUtc;

        public DateTime LastUpdateTimeUtc
        {
            get { return _lastUpdateTimeUtc; }
            set { _lastUpdateTimeUtc = value; }
        }
	

        private string _dBName;

        /// <summary>
        /// Gets or sets the name of the database
        /// </summary>
        /// <value>The name of the database.</value>
        public string DBName
        {
            get { return _dBName; }
            set { _dBName = value; }
        }

        private string _dBDescription;

        /// <summary>
        /// Gets or sets the database description.
        /// </summary>
        /// <value>The database description.</value>
        public string DBDescription
        {
            get { return _dBDescription; }
            set { _dBDescription = value; }
        }
    }
}
