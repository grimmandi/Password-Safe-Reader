﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO;
using System.Windows.Shapes;
using Microsoft.Phone.Tasks;
using Password_Safe_Reader_Constants;

namespace Password_Safe_Reader__Silverlight__
{
    public partial class InfoPage : PhoneApplicationPage
    {
        public InfoPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            InfoScroller.Content = TextToXaml("info.txt");
        }

        private static StackPanel TextToXaml(string filename)
        {
            var panel = new StackPanel();
            var resourceStream = Application.GetResourceStream(new Uri(filename, UriKind.RelativeOrAbsolute));
            var style = "PhoneTextLargeStyle";
            var size = 22;
            if (resourceStream != null)
            {
                using (var reader = new StreamReader(resourceStream.Stream))
                {
                    string line;
                    do
                    {
                        line = reader.ReadLine();
                        if (string.IsNullOrEmpty(line))
                        {
                            panel.Children.Add(new Rectangle { Height = 20.0 });
                        }
                        else if (line.Contains("{Style:"))
                        {
                            switch (line)
                            {
                                case "{Style:Title}":
                                    style = "PhoneTextLargeStyle";
                                    size = 28;
                                    break;

                                default:
                                    style = "PhoneTextNormalStyle";
                                    size = 22;
                                    break;
                            }
                        }
                        else
                        {
                            var textBlock = new TextBlock
                            {
                                TextWrapping = TextWrapping.Wrap,
                                Text = line,
                                Style = (Style)Application.Current.Resources[style],
                                FontSize = size,
                            };
                            panel.Children.Add(textBlock);
                        }
                    } while (line != null);
                }
            }
            return panel;
        }

        private void HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            WebBrowserTask webBrowserTask = new WebBrowserTask();

            try
            {
                webBrowserTask.Uri = new Uri(PasswordSafeConstants.PRIVACY_POLICY_URL, UriKind.Absolute);
                webBrowserTask.Show();
            }
            catch (Exception)
            {
                // nothing to do
            }
        }
    }
}