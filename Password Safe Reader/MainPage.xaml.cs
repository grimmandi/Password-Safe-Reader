﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Navigation;
using Windows.ApplicationModel.Activation;
using Windows.Storage;
using Windows.Storage.Pickers;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Password_Safe_Reader__Silverlight_;
using Password_Safe_Reader__Silverlight_.Model;
using GestureEventArgs = System.Windows.Input.GestureEventArgs;
using Windows.Phone.Storage.SharedAccess;
using Windows.Storage.FileProperties;
using Password_Safe_Reader_Constants;
using PasswordSafe;
using PasswordSafe.Crypto;
using Password_Safe_Reader__Silverlight__.ViewModel;
using System.Collections.ObjectModel;
using Windows.Security.Credentials;

namespace Password_Safe_Reader__Silverlight__
{
    public partial class MainPage : PhoneApplicationPage
    {
        private static bool IsAppInstancePreserved { get; set; }
        FileStream safeInputStream;
        private PasswordSafeViewModel passwordSafeViewModel;
        private ObservableCollection<RecentSafes> recentSafes { get; set; }

        // Constructor
        public MainPage()
        {
            InitializeComponent();
            passwordSafeViewModel = new PasswordSafeViewModel();
        }


        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            recentSafes = new ObservableCollection<RecentSafes>();

            // on first open init the keep open settings
            var localSettings = ApplicationData.Current.LocalSettings;

            IReadOnlyList<StorageFile> filesInFolder = await ApplicationData.Current.LocalFolder.GetFilesAsync();
            foreach (StorageFile file in filesInFolder)
            {
                if (file.FileType.Equals(".psafe3") && !recentSafes.Contains(new RecentSafes(file.Name)))
                {
                    FileInfo fileInfo = new FileInfo(file.Path);
                    if (! localSettings.Values.ContainsKey(file.Name))
                    {
                        localSettings.Values.Add(file.Name, file.Name);
                        recentSafes.Add(new RecentSafes(file.Name, file.Name, fileInfo.LastWriteTime));
                    }
                    else
                    {
                        recentSafes.Add(new RecentSafes(file.Name, (string)localSettings.Values[file.Name], fileInfo.LastWriteTime));
                    }
                }
            }

            // if no setting is available for the keep open setting default it to false
            if (! localSettings.Values.ContainsKey(PasswordSafeConstants.KEEP_OPEN_SETTING_NAME))
            {
                localSettings.Values[PasswordSafeConstants.KEEP_OPEN_SETTING_NAME] = false;
            }

            // if no setting is available for keep open timeout default it to 5 minutes
            if (! localSettings.Values.ContainsKey(PasswordSafeConstants.KEEP_OPEN_TIMEOUT_NAME))
            {
                localSettings.Values[PasswordSafeConstants.KEEP_OPEN_TIMEOUT_NAME] = 5;
            }

            // navigation string
            IDictionary<string, string> queryStrings = this.NavigationContext.QueryString;

            // remove previous page from history when safe was closed due to a timeout
            if (queryStrings.ContainsKey("reopen"))
            {
                var tmpBackStack = NavigationService.BackStack;
                var pagestoremove = 0;
                foreach (var backpage in tmpBackStack)
                {
                    if (!backpage.Source.ToString().Contains("MainPage"))
                        pagestoremove++;
                    else
                        break;
                }
                for (int i = 0; i < pagestoremove; i++)
                {
                    NavigationService.RemoveBackEntry();
                }
            }

            // app is opened by another app with an associated safe
            if (queryStrings.ContainsKey("fileToken"))
            {
                String fileID;
                queryStrings.TryGetValue("fileToken", out fileID);
                queryStrings.Remove(new KeyValuePair<string, string>("fileToken", fileID));
                string incomingFileName = SharedStorageAccessManager.GetSharedFileName(fileID);
                var destinationfile = await SharedStorageAccessManager.CopySharedFileAsync(ApplicationData.Current.LocalFolder, incomingFileName, NameCollisionOption.GenerateUniqueName, fileID);
                FileInfo fileInfo = new FileInfo(destinationfile.Path);
                if (!recentSafes.Contains(new RecentSafes(destinationfile.Name)))
                {
                    if (! localSettings.Values.ContainsKey(destinationfile.Name))
                    {
                        localSettings.Values.Add(destinationfile.Name, destinationfile.Name);
                        recentSafes.Add(new RecentSafes(destinationfile.Name, destinationfile.Name, fileInfo.LastWriteTime));
                    }
                    else
                    {
                        recentSafes.Add(new RecentSafes(destinationfile.Name, (string)localSettings.Values[destinationfile.Name], fileInfo.LastWriteTime));
                    }
                }
            }

            var app = App.Current as App;
            if (app.FilePickerContinuationArgs != null)
            {
                ContinueFileOpenPicker(app.FilePickerContinuationArgs);
                app.FilePickerContinuationArgs = null;
            }
            if (app.FileSaveContinuationArgs != null)
            {
                NavigationService.Navigate(new Uri(PasswordSafeConstants.LIST_PAGE_URL, UriKind.Relative));
            }

            if (PhoneApplicationService.Current.State.ContainsKey(PasswordSafeConstants.PASSWORD_SAFE_STATE_NAME))
            {
                passwordSafeViewModel = (PasswordSafeViewModel)PhoneApplicationService.Current.State[PasswordSafeConstants.PASSWORD_SAFE_STATE_NAME];
            }

            // Set the data context for the Item view.
            RecentSafesGrid.DataContext = recentSafes;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            StateUtilities.IsLaunching = false;

            // serialize the safe manually and null it, as the library does not support serialization
            passwordSafeViewModel.saveSafe();

            // Save the opened safe
            PhoneApplicationService.Current.State[PasswordSafeConstants.PASSWORD_SAFE_STATE_NAME] = passwordSafeViewModel;
        }

        public async void ContinueFileOpenPicker(FileOpenPickerContinuationEventArgs args)
        {
            if (args.Files.Count > 0)
            {
                try
                {
                    StorageFile file = args.Files[0];
                    var destinationfile = await file.CopyAsync(ApplicationData.Current.LocalFolder, file.Name, NameCollisionOption.GenerateUniqueName);

                    // when I file is opened from DropBox or OneDrive it is Read-Only
                    // creating a new file, which is writable
                    if ((destinationfile.Attributes & Windows.Storage.FileAttributes.ReadOnly) == Windows.Storage.FileAttributes.ReadOnly)
                    {
                        // using a weird name to prevent collision
                        await destinationfile.RenameAsync(destinationfile.Name + PasswordSafeConstants.READ_ONLY_TEMP_EXTENSION, NameCollisionOption.GenerateUniqueName);
                        StorageFile tmpFile = await ApplicationData.Current.LocalFolder.CreateFileAsync(file.Name, CreationCollisionOption.GenerateUniqueName);
                        var tmpInputStream = await FileIO.ReadBufferAsync(destinationfile);
                        var tmpOutputStrem = FileIO.WriteBufferAsync(tmpFile, tmpInputStream);
                        await destinationfile.DeleteAsync();
                        destinationfile = tmpFile;
                    }


                    BasicProperties basicProperties = await file.GetBasicPropertiesAsync();
                    var localSettings = ApplicationData.Current.LocalSettings;
                    if (!recentSafes.Contains(new RecentSafes(destinationfile.Name)))
                    {
                        if (!localSettings.Values.ContainsKey(destinationfile.Name))
                        {
                            localSettings.Values.Add(destinationfile.Name, destinationfile.Name);
                            recentSafes.Add(new RecentSafes(destinationfile.Name, destinationfile.Name, basicProperties.DateModified));
                        }
                        else
                        {
                            recentSafes.Add(new RecentSafes(destinationfile.Name, (string)localSettings.Values[destinationfile.Name], basicProperties.DateModified));
                        }
                    }
                }
                catch (Exception)
                {
                    throw;
                } 
            }
        }

        private void AppBarSettings_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri(PasswordSafeConstants.SETTINGS_PAGE_URL, UriKind.Relative));
        }

        private void Edit_Click(object sender, RoutedEventArgs e)

        {
            var selectedSafe = (sender as MenuItem).DataContext as RecentSafes;

            var localSettings = ApplicationData.Current.LocalSettings;

            // Save the recent safes
            PhoneApplicationService.Current.State[PasswordSafeConstants.EDIT_SAFE_NAME] = selectedSafe.FileName;

            NavigationService.Navigate(new Uri(PasswordSafeConstants.SAFE_EDIT_PAGE_URL, UriKind.Relative));
        }

        private async void Delete_Click(object sender, RoutedEventArgs e)

        {
            var selected = (sender as MenuItem).DataContext as RecentSafes;
            var localSettings = ApplicationData.Current.LocalSettings;

            MessageBoxResult confirmation = MessageBox.Show(PasswordSafeConstants.DELETE_SAFE_CONFIRMATION + selected.FileName, PasswordSafeConstants.DELETE_SAFE_CAPTION, MessageBoxButton.OKCancel);
            if (confirmation == MessageBoxResult.OK)
            {
                try
                {
                    recentSafes.Remove(selected);
                    var selectedSafe = await ApplicationData.Current.LocalFolder.GetFileAsync(selected.FileName);
                    await selectedSafe.DeleteAsync(StorageDeleteOption.PermanentDelete);
                    MessageBox.Show("Safe " + selected.FileName + " deleted.");
                    if (localSettings.Values.ContainsKey(selected.FileName))
                    {
                        localSettings.Values.Remove(selected.FileName);
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show(PasswordSafeConstants.DELETE_SAFE_ERROR_TEXT);
                }
            }
        }

        private async Task OpenSafe(string fileName, bool ignoreQuickOpen=false)
        {
            StorageFile file = await ApplicationData.Current.LocalFolder.GetFileAsync(fileName);
            var crypto = (IPasswordSafeCrypto)new PasswordSafeCrypto();
            Safe safe;
            PasswordVault pwVault = new PasswordVault();
            var password = string.Empty;
            bool quickOpenAvailable = false;

            try
            {
                if((file.Attributes & Windows.Storage.FileAttributes.ReadOnly)  == Windows.Storage.FileAttributes.ReadOnly)
                {
                    safeInputStream = File.Open(file.Path, FileMode.Open, FileAccess.Read);
                }
                else
                {
                    safeInputStream = File.Open(file.Path, FileMode.Open);
                }

            }
            catch (UnauthorizedAccessException)
            {
                throw;
            }
            catch (Exception)
            {
                MessageBox.Show("Could not open safe");
                throw;
            }
            if (safeInputStream == null)
            {
                MessageBox.Show("No Safe selected.");
            }
            else
            {
                Stream pwsafeStream = safeInputStream;
                try
                {
                    try
                    {
                        if ( ( pwVault.Retrieve(PasswordSafeConstants.PASSWORD_VAULT_RESOURCE, fileName + PasswordSafeConstants.QUICK_OPEN_EXTENSION).Password == PopupPasswordBox.Password ||
                             pwVault.Retrieve(PasswordSafeConstants.PASSWORD_VAULT_RESOURCE, fileName + PasswordSafeConstants.QUICK_OPEN_PASSWORD_EXTENSION).Password == PopupPasswordBox.Password ) &&
                             ! ignoreQuickOpen )
                        {
                            password = pwVault.Retrieve(PasswordSafeConstants.PASSWORD_VAULT_RESOURCE, fileName + PasswordSafeConstants.QUICK_OPEN_PASSWORD_EXTENSION).Password;
                            quickOpenAvailable = true;
                        } else
                        {
                            password = PopupPasswordBox.Password;
                        }
                    }
                    catch (Exception)
                    {
                        password = PopupPasswordBox.Password;
                    }
                    safe = Safe.Load(crypto, pwsafeStream, password);
                    passwordSafeViewModel.safe = safe;
                    pwVault.Add(new PasswordCredential(PasswordSafeConstants.PASSWORD_VAULT_RESOURCE, fileName, password));
                }
                catch (Exception)
                {
                    safeInputStream.Close();
                    if (quickOpenAvailable)
                    {
                        var gotoQuickOpenSetting = MessageBox.Show("Warning, your Quick Open password might be wrong.\nNavigate to Quick Open Settings ?", "", MessageBoxButton.OKCancel);
                        if (gotoQuickOpenSetting == MessageBoxResult.OK)
                        {
                            NavigationService.Navigate(new Uri(PasswordSafeConstants.PASSWORDSETTINGS_PAGE_URL, UriKind.Relative));
                        } else { 
                            await OpenSafe(fileName, true);
                        }
                    } else
                    {
                        throw;
                    }
                } finally
                {
                    safeInputStream.Close();
                }
            }
        }

        private void AppBarAdd_OnClick(object sender, EventArgs e)
        {
            FileOpenPicker openPicker = new FileOpenPicker();
            openPicker.ViewMode = PickerViewMode.List;
            openPicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
            openPicker.FileTypeFilter.Add(".psafe3");

            // Launch file open picker and caller app is suspended and may be terminated if required
            openPicker.PickSingleFileAndContinue();
        }

        private void PopupOpenButton_OnClick(object sender, RoutedEventArgs e)
        {
            Open();
        }

        private void PopupCancelButton_OnClick(object sender, RoutedEventArgs e)
        {
            Popup.IsOpen = false;
            PopupPasswordBox.Password = "";
        }

        private void UIElement_OnTap(object sender, GestureEventArgs e)
        {
            var selectedSafe = (sender as ListBox).SelectedItem as RecentSafes;

            if (selectedSafe == null) return;
            if (passwordSafeViewModel.fileName != null && passwordSafeViewModel.lastSafeName != selectedSafe.FileName)
            {
                passwordSafeViewModel.lastSafeName = passwordSafeViewModel.fileName;
            }
            else if (passwordSafeViewModel.fileName == null)
            {
                passwordSafeViewModel.lastSafeName = selectedSafe.FileName;
            }
            passwordSafeViewModel.fileName = selectedSafe.FileName;
            passwordSafeViewModel.title = selectedSafe.Title ?? selectedSafe.FileName;

            Popup.IsOpen = true;
        }

        private void PopupPasswordBox_onKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Open();
            }
        }

        private void AppBarInfo_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri(PasswordSafeConstants.INFO_PAGE_URL, UriKind.Relative));
        }

        private async void Open()
        {
            // Close the popup.
            Popup.IsOpen = false;
            try
            {
                await OpenSafe(passwordSafeViewModel.fileName);
                PopupPasswordBox.Password = "";
                NavigationService.Navigate(new Uri(PasswordSafeConstants.LIST_PAGE_URL, UriKind.Relative));
            }
            catch (Exception exception)
            {
                string errortext = "Wrong Password";
                if (exception.Message.Contains("denied"))
                {
                    errortext = "File could not be opened.";
                }
                PopupPasswordBox.Password = "";
                MessageBox.Show(errortext);
            }
        }

        private void Popup_Opened(object sender, EventArgs e)
        {
            PopupPasswordBox.Focus();
        }
    }
}