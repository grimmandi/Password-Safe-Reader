﻿using System.Collections.Generic;

namespace Password_Safe_Reader_Constants
{
    public static class PasswordSafeConstants
    {
        // Constants for Keep Open Setting
        public const string KEEP_OPEN_SETTING_NAME = "KeepOpenSetting";
        public const string KEEP_OPEN_TIMEOUT_NAME = "KeepOpenTimeout";
        
        // Constants for State Saving
        public const string APP_STATE_NAME = "AppViewModel";
        public const string PASSWORD_SAFE_STATE_NAME = "PasswordSafeViewModel";
        public const string LAST_OPENED_DATETIME_NAME = "LastOpenedDatetime";
        public const string EDIT_SAFE_NAME = "EditSafeName";
        public const string PASSWORD_VAULT_RESOURCE = "PasswordSafe";

        // Constants for Isolated Storage
        public const string SAFE_STORAGE_NAME = "storedSafe";
        public const string ITEM_STORAGE_NAME = "storedItem";

        // Constants for App Navigation
        public const string MAIN_PAGE_URL = "/MainPage.xaml";
        public const string MAIN_PAGE_REOPEN_URL = "/MainPage.xaml?reopen=true";
        public const string LIST_PAGE_URL = "/BetterListPage.xaml";
        public const string RECORD_PAGE_URL = "/RecordPage.xaml";
        public const string INFO_PAGE_URL = "/InfoPage.xaml";
        public const string SAFE_EDIT_PAGE_URL = "/EditPage.xaml";
        public const string SETTINGS_PAGE_URL = "/SettingsPage.xaml";
        public const string ADDRECORD_PAGE_URL = "/AddRecordPage.xaml";
        public const string PRIVACY_POLICY_URL = "https://grimmandi.gitlab.io/privacy-policy.html";
        public const string PASSWORDSETTINGS_PAGE_URL = "/PasswordSettingsPage.xaml";

        // Constants for Passwords
        public const string PASSWORD_OBFUSCATION_TEXT = "********";
        public const int GENERATED_PASSWORD_LENGTH = 12;

        // Constants for File handling
        public const string QUICK_OPEN_EXTENSION = "-quick-gsMMxp9i";
        public const string QUICK_OPEN_PASSWORD_EXTENSION = "-stored-J7DmCnkS";
        public const string READ_ONLY_TEMP_EXTENSION = "read-Only-temp";

        // Constants for Texts
        public const string USER_COPIED_TEXT = "Username copied.";
        public const string PASSWORD_COPIED_TEXT = "Password copied.\nHold Password to show in cleartext.";
        public const string URL_OPEN_ERROR_TEXT = "Could not open the URL.";
        public const string URL_HTTP_PREFIX_TEXT = "http://";
        public const string URL_HTTPS_PREFIX_TEXT = "https://";
        public const string URL_COPIED_TEXT = "URL copied.\nHold to open in Browser.";
        public const string PASSWORD_RECORD_RETRIEVAL_ERROR = "Error retrieving the Password Record.";
        public const string SAFE_FILE_RENAME_ERROR_TEXT = "Safe file could not be renamed.";
        public const string SAFE_RETRIEVAL_ERROR_TEXT = "Could not retrieve Safe information.";
        public const string SAFE_SAVE_ERROR_TEXT = "Could not save the safe !";
        public const string DELETE_SAFE_CONFIRMATION = "Press ok if you want to delete:\n";
        public const string DELETE_SAFE_CAPTION = "Confirm deletion";
        public const string DELETE_SAFE_ERROR_TEXT = "Could not delete Safe.";
        public const string QUICK_OPEN_VERIFY_FAIL = "Please select a safe and fill in both passwords";
        internal const string QUICK_OPEN_SET_TEXT = "Quick Open set for: ";

        // New Entry defaults
        public static Dictionary<string, string> defaultContents = new Dictionary<string, string>()
        {
            { "TitleContent", "Title" },
            { "UsernameContent", "Username" },
            { "PasswordContent", "Password" },
            { "URLContent", "Url" },
            { "NotesContent", "Notes" }
        };
    }
}
