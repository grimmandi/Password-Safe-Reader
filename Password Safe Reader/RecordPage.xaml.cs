﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using Password_Safe_Reader__Silverlight__.ViewModel;
using GestureEventArgs = System.Windows.Input.GestureEventArgs;
using Password_Safe_Reader_Constants;
using Windows.Storage;

namespace Password_Safe_Reader__Silverlight__
{
    public partial class RecordPage : PhoneApplicationPage
    {
        private bool _isNewPageInstance;
        private PasswordSafeViewModel passwordSafeViewModel;

        public RecordPage()
        {
            InitializeComponent();
            _isNewPageInstance = true;
            passwordSafeViewModel = new PasswordSafeViewModel();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            // Save the state of the PasswordSafeViewModel
            PhoneApplicationService.Current.State[PasswordSafeConstants.PASSWORD_SAFE_STATE_NAME] = passwordSafeViewModel;
            passwordSafeViewModel.saveSelectedItem();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            // keep open setting handling
            var localSettings = ApplicationData.Current.LocalSettings;
            bool isKeepOpenTimeouted = true;
            
            if (PhoneApplicationService.Current.State.ContainsKey(PasswordSafeConstants.PASSWORD_SAFE_STATE_NAME))
            {
                passwordSafeViewModel = (PasswordSafeViewModel)PhoneApplicationService.Current.State[PasswordSafeConstants.PASSWORD_SAFE_STATE_NAME];
                if( ! passwordSafeViewModel.getSavedItem())
                {
                    MessageBox.Show(PasswordSafeConstants.PASSWORD_RECORD_RETRIEVAL_ERROR);
                    try
                    {
                        NavigationService.GoBack();
                        return;
                    }
                    catch (Exception)
                    {
                        NavigationService.Navigate(new Uri(PasswordSafeConstants.MAIN_PAGE_URL, UriKind.Relative));
                        return;
                    } 
                }
            }

            if (!PhoneApplicationService.Current.State.ContainsKey(PasswordSafeConstants.LAST_OPENED_DATETIME_NAME))
            {
                PhoneApplicationService.Current.State[PasswordSafeConstants.LAST_OPENED_DATETIME_NAME] = DateTime.Now;
                isKeepOpenTimeouted = false;
            }
            else
            {
                // keep the safe open, unless app was closed longer than the timeout setting.
                DateTime now = DateTime.Now;
                DateTime lastOpened = (DateTime)PhoneApplicationService.Current.State[PasswordSafeConstants.LAST_OPENED_DATETIME_NAME];
                double tmp = double.Parse(localSettings.Values[PasswordSafeConstants.KEEP_OPEN_TIMEOUT_NAME].ToString());
                DateTime lastOpenedPlusTimeout = lastOpened.AddMinutes(tmp);
                if (lastOpenedPlusTimeout.CompareTo(DateTime.Now) > 0)
                {
                    isKeepOpenTimeouted = false;
                }
                PhoneApplicationService.Current.State[PasswordSafeConstants.LAST_OPENED_DATETIME_NAME] = DateTime.Now;
            }

            // for safety, can be disabled by enabling keep safe open in settings page
            if ( ( ! (bool) localSettings.Values[PasswordSafeConstants.KEEP_OPEN_SETTING_NAME] ||
                    isKeepOpenTimeouted) &&
                ! _isNewPageInstance)
            {
                NavigationService.Navigate(new Uri(PasswordSafeConstants.MAIN_PAGE_REOPEN_URL, UriKind.Relative));
                return;
            }

            // display content when data present
            if (!string.IsNullOrEmpty(passwordSafeViewModel.selectedItem.Title))
                PageTitle.Text = passwordSafeViewModel.selectedItem.Title;

            if (!string.IsNullOrEmpty(passwordSafeViewModel.selectedItem.User))
            {
                UsernameContent.Text = passwordSafeViewModel.selectedItem.User;
                UsernameContent.Visibility = Visibility.Visible;
                UsernameLabel.Visibility = Visibility.Visible;
            }

            if (!string.IsNullOrEmpty(passwordSafeViewModel.selectedItem.Password))
            {
                PasswordContent.Text = PasswordSafeConstants.PASSWORD_OBFUSCATION_TEXT;
                PasswordContent.Visibility = Visibility.Visible;
                PasswordLabel.Visibility = Visibility.Visible;
            }

            if (!string.IsNullOrEmpty(passwordSafeViewModel.selectedItem.Url))
            {
                URLContent.Text = passwordSafeViewModel.selectedItem.Url;
                URLContent.Visibility = Visibility.Visible;
                URLLabel.Visibility = Visibility.Visible;
            }

            if (!string.IsNullOrEmpty(passwordSafeViewModel.selectedItem.Notes))
            {
                var panel = new StackPanel();
                var textBlock = new TextBox
                {
                    TextWrapping = TextWrapping.Wrap,
                    Text = passwordSafeViewModel.selectedItem.Notes,
                    IsReadOnly = true,
                };
                panel.Children.Add(textBlock);

                Scroller.Content = panel;

                NotesLabel.Visibility = Visibility.Visible;
            }

            // Set _isNewPageInstance to false. If the user navigates back to this page
            // and it has remained in memory, this value will continue to be false.
            _isNewPageInstance = false;

        }

        private void UsernameContent_OnTap(object sender, GestureEventArgs e)
        {
            PopupText.Text = PasswordSafeConstants.USER_COPIED_TEXT;
            Popup.IsOpen = true;
            Clipboard.SetText(UsernameContent.Text);
        }

        private void PasswordContent_OnTap(object sender, GestureEventArgs e)
        {
            PopupText.Text = PasswordSafeConstants.PASSWORD_COPIED_TEXT;
            Popup.IsOpen = true;
            Clipboard.SetText(passwordSafeViewModel.selectedItem.Password);
        }

        private void PasswordContent_OnHold(object sender, GestureEventArgs e)
        {
            Popup.IsOpen = false;
            if (PasswordContent.Text.Contains(PasswordSafeConstants.PASSWORD_OBFUSCATION_TEXT))
            {
                PasswordContent.Text = passwordSafeViewModel.selectedItem.Password;
            }
            else
            {
                PasswordContent.Text = PasswordSafeConstants.PASSWORD_OBFUSCATION_TEXT;
            }
        }

        private void URLContent_OnHold(object sender, GestureEventArgs e)
        {
            string url = URLContent.Text;
            if (url.Length > 0)
            {
                if (!url.StartsWith(PasswordSafeConstants.URL_HTTP_PREFIX_TEXT) && !url.StartsWith(PasswordSafeConstants.URL_HTTPS_PREFIX_TEXT))
                {
                    url = PasswordSafeConstants.URL_HTTP_PREFIX_TEXT + url;
                }
                WebBrowserTask webBrowserTask = new WebBrowserTask();

                try
                {
                    webBrowserTask.Uri = new Uri(url, UriKind.Absolute);
                    webBrowserTask.Show();
                }
                catch (Exception)
                {
                    PopupText.Text = PasswordSafeConstants.URL_OPEN_ERROR_TEXT;
                    Popup.IsOpen = true;
                }

            }
        }

        private void URLContent_OnTap(object sender, GestureEventArgs e)
        {
            PopupText.Text = PasswordSafeConstants.URL_COPIED_TEXT;
            Popup.IsOpen = true;
            Clipboard.SetText(passwordSafeViewModel.selectedItem.Url);
        }

        private void PopupButton_OnTap(object sender, GestureEventArgs e)
        {
            Popup.IsOpen = false;
        }

    }
}