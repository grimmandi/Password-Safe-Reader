﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using Password_Safe_Reader__Silverlight__.ViewModel;
using GestureEventArgs = System.Windows.Input.GestureEventArgs;
using Password_Safe_Reader_Constants;
using Windows.Storage;
using System.Security.Cryptography;

namespace Password_Safe_Reader__Silverlight__
{
    public partial class AddRecordPage : PhoneApplicationPage
    {
        private PasswordSafeViewModel passwordSafeViewModel;

        public AddRecordPage()
        {
            InitializeComponent();
            passwordSafeViewModel = new PasswordSafeViewModel();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            PasswordSafe.Item newRecord = new PasswordSafe.Item();
            newRecord.CreatedTime = DateTime.Now;
            newRecord.Uuid = Guid.NewGuid();
            newRecord.User = UsernameContent.Text;
            newRecord.Password = PasswordContent.Text;
            newRecord.Title = TitleContent.Text;
            newRecord.Url = URLContent.Text;
            newRecord.Notes = NotesContent.Text;

            passwordSafeViewModel.safe.Items.Add(newRecord);

            // Save the state of the PasswordSafeViewModel
            PhoneApplicationService.Current.State[PasswordSafeConstants.PASSWORD_SAFE_STATE_NAME] = passwordSafeViewModel;
            passwordSafeViewModel.saveSelectedItem();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            // keep open setting handling
            var localSettings = ApplicationData.Current.LocalSettings;
            
            if (PhoneApplicationService.Current.State.ContainsKey(PasswordSafeConstants.PASSWORD_SAFE_STATE_NAME))
            {
                passwordSafeViewModel = (PasswordSafeViewModel)PhoneApplicationService.Current.State[PasswordSafeConstants.PASSWORD_SAFE_STATE_NAME];
                if( ! passwordSafeViewModel.getSavedItem())
                {
                    MessageBox.Show(PasswordSafeConstants.PASSWORD_RECORD_RETRIEVAL_ERROR);
                    try
                    {
                        NavigationService.GoBack();
                        return;
                    }
                    catch (Exception)
                    {
                        NavigationService.Navigate(new Uri(PasswordSafeConstants.MAIN_PAGE_URL, UriKind.Relative));
                        return;
                    } 
                }
            }
        }
        

        private void PopupButton_OnTap(object sender, GestureEventArgs e)
        {
            Popup.IsOpen = false;
        }

        private void RandomPasswordButton_Click(object sender, RoutedEventArgs e)
        {
            PasswordGenerator pwGen = new PasswordGenerator();
            PasswordContent.Text = pwGen.Generate();
        }

        private void Content_GotFocus(object sender, RoutedEventArgs e)
        {
            if ((sender as TextBox).Text == PasswordSafeConstants.defaultContents[(sender as TextBox).Name])
            {
                (sender as TextBox).Text = "";
            }
        }

        private void Content_LostFocus(object sender, RoutedEventArgs e)
        {
            if((sender as TextBox).Text.Length == 0)
            {
                (sender as TextBox).Text = PasswordSafeConstants.defaultContents[(sender as TextBox).Name];
            }
        }
    }
}