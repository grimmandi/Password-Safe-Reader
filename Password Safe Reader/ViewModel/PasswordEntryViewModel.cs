﻿using Axantum.PasswordSafe;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Password_Safe_Reader_Constants;

namespace Password_Safe_Reader__Silverlight__.ViewModel
{
    public class PasswordEntryViewModel
    {
        public ObservableCollection<PasswordSafeRecord> _passwordSafeRecords;
        public int _selectedIndex = -1;

        public PasswordEntryViewModel()
        {
            PasswordSafeRecords = new ObservableCollection<PasswordSafeRecord>();
        }

        public ObservableCollection<PasswordSafeRecord> PasswordSafeRecords
        {
            get { return _passwordSafeRecords; }
            set { _passwordSafeRecords = value; }
        }

        public int SelectedIndex
        {
            get
            {
                return _selectedIndex;
            }

            set
            {
                _selectedIndex = value;
            }
        }

        public void SavePasswordSafeRecords()
        {
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            settings[PasswordSafeConstants.PASSWORD_SAFE_RECORDS_STORAGE_NAME] = PasswordSafeRecords;

            settings.Save();
            Debug.WriteLine("Finished saving passwordSafeRecords");

        }

        public bool GetPasswordSafeRecords()
        {
            ObservableCollection<PasswordSafeRecord> a = new ObservableCollection<PasswordSafeRecord>();
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;

            settings.TryGetValue(PasswordSafeConstants.PASSWORD_SAFE_RECORDS_STORAGE_NAME, out a);
            if (a == null)
            {
                PasswordSafeRecords = new ObservableCollection<PasswordSafeRecord>();
                return false;
            }
            PasswordSafeRecords = a;
            Debug.WriteLine("Got records from storage");
            return true;
        }
    }
}
