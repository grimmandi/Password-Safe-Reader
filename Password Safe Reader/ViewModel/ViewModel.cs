﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO.IsolatedStorage;
using Windows.Storage;
using Axantum.PasswordSafe;
using Password_Safe_Reader__Silverlight_.Model;
using Windows.Storage.FileProperties;
using System.Globalization;
using Password_Safe_Reader_Constants;

namespace Password_Safe_Reader__Silverlight__.ViewModel
{
    public class ViewModel
    {
        public ObservableCollection<PasswordSafeRecord> PasswordSafeRecords { get; set; }
        public ObservableCollection<RecentSafes> RecentSafes { get; set; }
        public string SafeFile { get; set; }
        public string LastSafeFile { get; set; }
        public string SafeTitle { get; set; }
        public string Password { get; set; }
        public RecentSafes SelectedSafe { get; set; }


        public ViewModel()
        {
            PasswordSafeRecords = new ObservableCollection<PasswordSafeRecord>();
            RecentSafes = new ObservableCollection<RecentSafes>();
        }

        public async void GetRecentSafes()
        {
            if (IsolatedStorageSettings.ApplicationSettings.Count > 0)
            {
                GetSavedRecentSafesRecords();
            }
            // Get the files in the current folder.
            IReadOnlyList<StorageFile> filesInFolder =
                     await ApplicationData.Current.LocalFolder.GetFilesAsync();

            // Iterate over the results and print the list of files
            // to the Visual Studio Output window.
            foreach (StorageFile file in filesInFolder)
            {
                if (file.FileType.Equals(".psafe3") && !RecentSafes.Contains(new RecentSafes(file.Name)))
                {
                    BasicProperties fileProperties = await file.GetBasicPropertiesAsync();
                    RecentSafes.Add(new RecentSafes(file.Name, file.Name, fileProperties.DateModified));
                }
            }

        }


        private void GetSavedRecentSafesRecords()
        {
            ObservableCollection<RecentSafes> a = new ObservableCollection<RecentSafes>();
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;

            settings.TryGetValue(PasswordSafeConstants.RECENT_SAFE_STORAGE_NAME, out a);

            RecentSafes = a;
            Debug.WriteLine("Got recent safes from storage");
        }

        public void SaveRecentSafesRecords()
        {
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;

            settings[PasswordSafeConstants.RECENT_SAFE_STORAGE_NAME] = RecentSafes;

            settings.Save();
            Debug.WriteLine("Finished saving recentSafeRecords");
        }

        public void SavePasswordSafeRecords()
        {
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            settings[PasswordSafeConstants.RECORDS_STORAGE_NAME] = PasswordSafeRecords;

            settings.Save();
            Debug.WriteLine("Finished saving passwordSafeRecords");

        }

        public bool GetPasswordSafeRecords()
        {
            ObservableCollection<PasswordSafeRecord> a = new ObservableCollection<PasswordSafeRecord>();
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;

            settings.TryGetValue(PasswordSafeConstants.RECORDS_STORAGE_NAME, out a);
            if (a == null)
            {
                PasswordSafeRecords = new ObservableCollection<PasswordSafeRecord>();
                return false;
            }
            PasswordSafeRecords = a;
            Debug.WriteLine("Got records from storage");
            return true;
        }

    }
}
