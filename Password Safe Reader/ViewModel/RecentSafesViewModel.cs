﻿using System;
using Password_Safe_Reader__Silverlight_.Model;
using Password_Safe_Reader_Constants;
using System.Collections.ObjectModel;
using System.IO.IsolatedStorage;
using System.Runtime.Serialization;
using Windows.Storage;
using System.Collections.Generic;
using Windows.Storage.FileProperties;

namespace Password_Safe_Reader__Silverlight__.ViewModel
{
    [DataContract]
    class RecentSafesViewModel
    {
        public ObservableCollection<RecentSafes> recentSafes { get; set; }
        public RecentSafes selectedResentSafe { get; set; }
        private ApplicationDataContainer settings = ApplicationData.Current.LocalSettings;

        public RecentSafesViewModel()
        {
            selectedResentSafe = new RecentSafes();
            recentSafes = new ObservableCollection<RecentSafes>();
        }

        public async void GetRecentSafes()
        {
            ObservableCollection<RecentSafes> retrievedRecentSafes = new ObservableCollection<RecentSafes>();

            if (settings.Values.ContainsKey(PasswordSafeConstants.RECENT_SAFE_STORAGE_NAME))
            {
                if (settings.Values[PasswordSafeConstants.RECENT_SAFE_STORAGE_NAME] == null)
                {
                    recentSafes = new ObservableCollection<RecentSafes>();
                }
                else
                {
                    recentSafes = (ObservableCollection<RecentSafes>)settings.Values[PasswordSafeConstants.RECENT_SAFE_STORAGE_NAME];
                }
            }
            else
            {
                recentSafes = new ObservableCollection<RecentSafes>();
            }

            // In case we couldn't receive all safes from the serialized state
            // go through local folder and add all found psafe3 files
            IReadOnlyList<StorageFile> filesInFolder = await ApplicationData.Current.LocalFolder.GetFilesAsync();
            foreach (StorageFile file in filesInFolder)
            {
                if (file.FileType.Equals(".psafe3") && !recentSafes.Contains(new RecentSafes(file.Name)))
                {
                    BasicProperties fileProperties = await file.GetBasicPropertiesAsync();
                    recentSafes.Add(new RecentSafes(file.Name, file.Name, fileProperties.DateModified));
                }
            }

        }

        // store the recentSafes for later usage
        public void SaveRecentSafesRecords()
        {
            if (settings.Containers.ContainsKey(PasswordSafeConstants.RECENT_SAFE_CONTAINER_NAME))
            {
                settings.Containers[PasswordSafeConstants.RECENT_SAFE_CONTAINER_NAME].Values[PasswordSafeConstants.RECENT_SAFE_STORAGE_NAME] = recentSafes;
            }
            else
            {
                ApplicationDataContainer container = settings.CreateContainer(PasswordSafeConstants.RECENT_SAFE_CONTAINER_NAME, ApplicationDataCreateDisposition.Always);
                settings.Containers[PasswordSafeConstants.RECENT_SAFE_CONTAINER_NAME].Values[PasswordSafeConstants.RECENT_SAFE_STORAGE_NAME] = recentSafes;
            }

            //settings[PasswordSafeConstants.RECENT_SAFE_STORAGE_NAME] = recentSafes;
            //if (settings.Contains(PasswordSafeConstants.RECENT_SAFE_STORAGE_NAME))
            //{
            //    settings[PasswordSafeConstants.RECENT_SAFE_STORAGE_NAME] = recentSafes;
            //}
            //else
            //{
            //    settings.Add(PasswordSafeConstants.RECENT_SAFE_STORAGE_NAME, recentSafes);
            //}
            //settings.Save();

        }
    }
}
