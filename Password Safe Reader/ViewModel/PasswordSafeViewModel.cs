﻿using PasswordSafe;
using System.IO.IsolatedStorage;
using Password_Safe_Reader_Constants;
using System.Runtime.Serialization;

namespace Password_Safe_Reader__Silverlight__.ViewModel
{
    [DataContract]
    class PasswordSafeViewModel
    {
        public Safe safe { get; set; }
        public Item selectedItem { get; set; }
        public string title { get; set; }
        [DataMember(Name = "fileName")]
        public string fileName { get; set; }
        public string lastSafeName { get; set; }
        public bool readOnly { get; set; }

        public PasswordSafeViewModel()
        {

        }

        public bool getSavedItem()
        {
            try
            {
                selectedItem = (Item) getObject(PasswordSafeConstants.ITEM_STORAGE_NAME);
            }
            catch (System.Exception)
            {
                return false;
            }
            return true;
        }

        public void saveSelectedItem()
        {
            save(PasswordSafeConstants.ITEM_STORAGE_NAME, selectedItem);
        }

        public bool getSavedSafe()
        {
            // Try to get stored safe.
            try
            {
                safe = (Safe) getObject(PasswordSafeConstants.SAFE_STORAGE_NAME);
            }
            catch (System.Exception)
            {
                // retrieval not successful
                return false;
            }
            return true;
        }

        public void saveSafe()
        {
            save(PasswordSafeConstants.SAFE_STORAGE_NAME,safe);
        }

        public void save(string target, object toSave)
        {
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;

            settings[target] = toSave;

            settings.Save();
        }

        public object getObject(string toRetrieve)
        {
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            object retrievedObject;

            // Try to get stored safe.
            try
            {
                settings.TryGetValue(toRetrieve, out retrievedObject);
                return retrievedObject;
            }
            catch (System.Exception)
            {
                // retrieval not successful
                throw;
            }
        }
    }
}
